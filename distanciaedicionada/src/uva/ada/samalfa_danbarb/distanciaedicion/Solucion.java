package uva.ada.samalfa_danbarb.distanciaedicion;
import java.util.ArrayList;

/**
 * Implementación auxiliar de una Solución al problema de distancia mínima
 * para encapsular los resultados necesarios.
 * 
 * @author Samuel Alfageme Sainz (samalfa)
 * @author Daniel Barba Gutiérrez (danbarb)
 */
public class Solucion{
	
	private ArrayList<String> listaPasos;
	private int distancia;
	
	public Solucion(){
		listaPasos = new ArrayList<String>();
		distancia = 0;
	}
	
	/**
	 * @return Distancia Minima de Edicion de una Solucion
	 */
	public int getDistancia(){
		return distancia;
	}
	
	/**
	 * Fija el valor de la distancia minima de edicion de una Solucion.
	 * 
	 * @param d - la distancia minima de edicion
	 */
	public void setDistancia(int d){
		distancia = d;
	}
	
	/**
	 * Añade un nuevo paso en la secuencia de obtención de la Solución
	 * 
	 * @param op - el tipo de operación a realizar de entre las 3 que sólo 
	 * afectan a un caracter.
	 * @param arg1 - el caracter implicado en dicha operación
	 */
	public void addPaso(Operacion op, char arg1){
		listaPasos.add(op.toString() + " " + arg1 + ".");
	}
	
	/**
	 * Sobrecarga del método <code>addPaso</code> para el caso en el que la
	 * operación es una Sustitución en la que queremos conservar tanto el valor
	 * sustituido como el que lo sustituye.
	 * 
	 * @param op - el tipo de operación: SUSTITUIR 
	 * @param arg1 - el caracter sustituido
	 * @param arg2 - el caracter sustituidor
	 */
	public void addPaso(Operacion op, char arg1, char arg2){
		listaPasos.add(op.toString() + " " + arg1 + " por " + arg2 + ".");
	}
	
	/**
	 * Por simplificar la llamada:
	 * @param p - 
	 */
	public void addPaso(Paso p){
		String pasoStr = p.op.toString() + " " + p.c1;
		if(p.op.equals(Operacion.SUSTITUIR))
			pasoStr = pasoStr + " por " + p.c2 + ".";
		else
			pasoStr = pasoStr + ".";
		listaPasos.add(pasoStr);
		if (!p.op.equals(Operacion.COPIAR)){
			distancia++;
		}
	}
	
	/**
	 * Elimina de la secuencia el ultimo paso insertado.
	 */
	public void removePaso(){
		String s = listaPasos.remove(listaPasos.size()-1);
		if (!s.contains("COPIAR")){
			distancia--;
		}
	}
	
	/**
	 * Enumeración de las diferentes operaciones permitidas: Insertar, Borrar, 
	 * Sustituir y Copiar.
	 */
	public enum Operacion{
		INSERTAR,
		BORRAR,
		SUSTITUIR,
		COPIAR;
	}
	
	public static class Paso{
		Operacion op;
		char c1, c2;
		
		public Paso(Operacion op, char c1){
			this.op = op;
			this.c1 = c1;
		}
		public Paso(Operacion op, char c1, char c2){
			this(op,c1);
			this.c2 = c2;
		}
	}
	
	/**
	 * Devuelve la representación en String del contenido de la secuencia de 
	 * pasos de la solución.
	 */
	@Override
	public String toString(){
		String res = "Operaciones:\n";
		String s;
		for (int i = listaPasos.size()-1; i >= 0; i--){
			s = listaPasos.get(i);
			res = res + s + "\n";
		}
		return res;
	}
}