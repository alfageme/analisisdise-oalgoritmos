package uva.ada.samalfa_danbarb.palabrasEncadenadas;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Práctica 3 : Análisis y Diseño de Algoritmos - Curso 2014/2015
 * 
 * Desarrollo de un algoritmo para la resolución del juego de las palabras 
 * encadenadas, consistente en sustituir una palabra por otra, modificando una 
 * unica letra cada vez y obteniendo en cada paso una palabra real de un 
 * diccionario predefinido.
 * 
 * @author Samuel Alfageme Sainz (samalfa)
 * @author Daniel Barba Gutiérrez(danbarb)
 */
public class PalabrasEncadenadas {
	
	private static final String OUTPUT = "salida_p3_samalfa_danbarb.txt";
	
	public static void main(String[] args) throws Exception{
		
		String[] tupla = leeFichero("entrada.txt");
		String[] diccionario = leeFichero("diccionario.txt");

		String palabraInicio   = tupla[0];
		String palabraObjetivo = tupla[1];
		
		int tamFiltrado = palabraInicio.length();
		if(tamFiltrado != palabraObjetivo.length())
			throw new IllegalArgumentException("El fichero de entrada "
					+ "introducido contiene palabras de distinta longitud");
		
		String[] subDiccionario  = filtrarDiccionario(diccionario,tamFiltrado);
		GrafoPalabras gp = new GrafoPalabras(subDiccionario);
		
		String [] caminoSolucion = gp.getCSolucion(palabraInicio
			 	, palabraObjetivo); 

		try{
			imprimeFichero(OUTPUT,caminoSolucion);
		}catch (Exception e){
			System.err.println(e);
		}	
	}
	
	/**
	 * Lee un fichero de texto, obteniendo como cadena todas las lineas
	 * existentes, despreciando las vacias.
	 * 
	 * @param ruta - ruta del fichero a leer.
	 * @return Array de String con todas las cadenas no vacias leidas
	 */
	public static String[] leeFichero(String ruta){
		ArrayList<String> listaCadenas = new ArrayList<String>();
		
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(ruta)));
		} catch (FileNotFoundException e) {
			System.err.println("Error al abrir el fichero.");
		}
		String s;
		try {
			while ((s = reader.readLine()) != null){
				if (!s.equals("")) 
					listaCadenas.add(s);
			}
			reader.close();
		} catch (IOException e) {
			System.err.println("Error al leer en fichero");
		}
		String[] cadenas = new String[listaCadenas.size()];
		cadenas = listaCadenas.toArray(cadenas);
		return cadenas;
	}
	
	/**
	 * Crea un fichero de texto (o sobreescribe uno existente) e imprime en él 
	 * las cadenas especificadas como argumento.
	 * 
	 * @param ruta - ruta al fichero en el que escribir.
	 * @param cadena - La cadena a añadir al fichero.
	 */
	public static void imprimeFichero(String ruta, String[] cadenas){
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(new BufferedWriter(new FileWriter(ruta)));
		} catch (IOException e) {
			System.err.println("Error en la escritura.");
		}
		for(String cadena : cadenas)
			writer.println(cadena);
		writer.flush();
		writer.close();
	}
	
	/**
	 * Inicializa un fichero, elimiando los posibles contenidos que pudiera 
	 * contener.
	 * 
	 * @param ruta - fichero a inicializar.
	 */
	public static void vaciaFichero(String ruta){
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(new BufferedWriter(
					new FileWriter(ruta, false)));
		} catch (IOException e) {
			System.err.println("Error al inicializar el fichero.");
		}
		writer.flush();
		writer.close();
	}
	
	/**
	 * Filtra un diccionario por tamaño
	 * 
	 * @param diccionario - el array total de palabras de entrada
	 * @param longitudFiltrado - la longitud con la que filtrar el diccionario
	 * @return El subconjunto de palabras del diccionario con la longitud 
	 * especificadas
	 */
	public static String[] filtrarDiccionario(String[] diccionario, 
			int longitudFiltrado){
		ArrayList<String> diccionarioFiltrado = new ArrayList<String>();
		for(int i=0; i<diccionario.length;i++){
			if(diccionario[i].length() == longitudFiltrado)
				diccionarioFiltrado.add(diccionario[i].toUpperCase());
		}
 		return diccionarioFiltrado.toArray(
 				new String[diccionarioFiltrado.size()]);
	}
	
}
